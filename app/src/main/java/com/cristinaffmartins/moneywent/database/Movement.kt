package com.cristinaffmartins.moneywent.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.cristinaffmartins.moneywent.utils.MovementCategories

@Entity(tableName = "movements",
    foreignKeys = arrayOf(ForeignKey(
        entity = Month::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("monthId"))))
data class Movement(
    var category: MovementCategories,
    var description: String,
    val monthId: Long,
    val isIncome: Boolean,
    var value: Float
) {
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var movementId: Long = 0
}