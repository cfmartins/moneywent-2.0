package com.cristinaffmartins.moneywent.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface MonthDao {
    @Insert
    fun addMonth(month: Month) : Long

    @Query("SELECT * FROM months WHERE monthNumber LIKE :monthNumber AND year LIKE :year")
    fun getMonth(monthNumber: Int, year: Int) : Month

    @Query("SELECT * FROM months WHERE id LIKE :monthId")
    fun getMonthById(monthId: Long) : Month
}