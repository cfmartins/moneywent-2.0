package com.cristinaffmartins.moneywent.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "months")
data class Month(
    val monthNumber: Int,
    val year: Int
) {
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var monthId: Long = 0
}