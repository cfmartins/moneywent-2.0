package com.cristinaffmartins.moneywent.database

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface MovementDao {
    @Query("SELECT * FROM movements WHERE monthId LIKE :monthId")
    fun getMovementsByMonthId(monthId: Long) : LiveData<List<Movement>>

    @Query("SELECT * FROM movements WHERE category LIKE :category AND monthId LIKE :monthId")
    fun getMovementsByCategoryAndMonthId(category: Int, monthId: Long) : LiveData<List<Movement>>

    @Query("SELECT * FROM movements WHERE isIncome = 1 AND monthId LIKE :monthId")
    fun getIncomesByMonthId(monthId: Long) : List<Movement>

    @Query("SELECT * FROM movements WHERE isIncome = 0 AND monthId LIKE :monthId")
    fun getExpensesByMonthId(monthId: Long) : List<Movement>

    @Query("SELECT * FROM movements WHERE id LIKE :movementId")
    fun getMovementById(movementId: Long) : LiveData<Movement>

    @Insert
    fun addMovement(movement: Movement)

    @Update
    fun editMovement(movement: Movement)

    @Delete
    fun deleteMovement(movement: Movement)
}