package com.cristinaffmartins.moneywent.view.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.cristinaffmartins.moneywent.R
import com.cristinaffmartins.moneywent.view.activities.MainActivity
import com.cristinaffmartins.moneywent.view.fragments.EditMovementFragment
import com.cristinaffmartins.moneywent.view.fragments.EditMovementFragment.Companion.EXPENSE
import com.cristinaffmartins.moneywent.view.fragments.EditMovementFragment.Companion.INCOME
import kotlinx.android.synthetic.main.add_movement_overlay.*

class AddMovementDialog : DialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, android.R.style.Theme_Translucent_NoTitleBar)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.add_movement_overlay, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        root.setOnClickListener{
            dismiss()
        }

        add_expense.setOnClickListener{
            (activity as MainActivity).addContentFragment(EditMovementFragment.newInstance(EXPENSE))
            dismiss()
        }

        add_income.setOnClickListener{
            (activity as MainActivity).addContentFragment(EditMovementFragment.newInstance(INCOME))
            dismiss()
        }
    }
}