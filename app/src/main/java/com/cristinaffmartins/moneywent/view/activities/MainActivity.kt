package com.cristinaffmartins.moneywent.view.activities

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.NumberPicker
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.cristinaffmartins.moneywent.R
import com.cristinaffmartins.moneywent.database.AppDatabase
import com.cristinaffmartins.moneywent.repository.MonthRepository
import com.cristinaffmartins.moneywent.utils.SharedPreferencesHelper
import com.cristinaffmartins.moneywent.utils.getDecimalInput
import com.cristinaffmartins.moneywent.utils.orZero
import com.cristinaffmartins.moneywent.view.fragments.MovementsFragment
import com.cristinaffmartins.moneywent.view.fragments.ReportFragment
import com.cristinaffmartins.moneywent.viewmodel.BalanceViewModel
import com.cristinaffmartins.moneywent.viewmodel.MonthViewModel
import com.cristinaffmartins.moneywent.viewmodel.MonthViewModelFactory
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.balance_popup_body.view.*
import kotlinx.android.synthetic.main.month_popup_body.view.*

class MainActivity : AppCompatActivity() {

    private lateinit var monthViewModel: MonthViewModel
    private lateinit var balanceViewModel: BalanceViewModel

    private val months: Array<String>
        get() {
            return arrayOf(
                getString(R.string.month_one), getString(R.string.month_two),
                getString(R.string.month_three), getString(R.string.month_four), getString(R.string.month_five),
                getString(R.string.month_six), getString(R.string.month_seven), getString(R.string.month_eight),
                getString(R.string.month_nine), getString(R.string.month_ten), getString(R.string.month_eleven),
                getString(R.string.month_twelve)
            )
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        SharedPreferencesHelper.init(applicationContext)

        setSupportActionBar(toolbar)

        balanceViewModel = ViewModelProviders.of(this).get(BalanceViewModel::class.java)

        MonthViewModelFactory(MonthRepository(AppDatabase.getInstance(this).monthDao())).also {
            monthViewModel = ViewModelProviders.of(this, it).get(MonthViewModel::class.java)
        }

        monthViewModel.month.observe(this, Observer {
            toolbar.subtitle = "${getMonthStringByMonthNumber(it.monthNumber)} ${it.year}"
            val currentFragment = supportFragmentManager.findFragmentById(R.id.content_fragment)
            if (currentFragment is MovementsFragment || currentFragment == null) {
                replaceContentFragment(MovementsFragment())
            } else {
                replaceContentFragment(ReportFragment())
            }
        })
    }

    private fun getMonthStringByMonthNumber(monthNumber: Int): String {
        return when(monthNumber) {
            1 -> months[0]
            2 -> months[1]
            3 -> months[2]
            4 -> months[3]
            5 -> months[4]
            6 -> months[5]
            7 -> months[6]
            8 -> months[7]
            9 -> months[8]
            10 -> months[9]
            11 -> months[10]
            12 -> months[11]
            else -> months[0]
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            R.id.action_list_report -> {
                switchBetweenListAndReport(item)
                true
            }
            R.id.action_balance -> {
                openBalancePopUp()
                true
            }
            R.id.action_month -> {
                openMonthSelectionPopUp()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun openBalancePopUp() {
        val view = layoutInflater.inflate(R.layout.balance_popup_body, null)
        view.apply {
            account.editText?.setText(balanceViewModel.getAccountValue().toString())
            savings.editText?.setText(balanceViewModel.getSavingsValue().toString())
            meal_card.editText?.setText(balanceViewModel.getMealCardValue().toString())
            wallet.editText?.setText(balanceViewModel.getWalletValue().toString())
        }

        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.apply {
            setTitle(getString(R.string.balances))
            setCancelable(true)
            setView(view)
            alertDialogBuilder.setPositiveButton(getString(R.string.save)) { dialog, _ ->
                balanceViewModel.setAccountValue(view.account.getDecimalInput())
                balanceViewModel.setSavingsValue(view.savings.getDecimalInput())
                balanceViewModel.setMealCardValue(view.meal_card.getDecimalInput())
                balanceViewModel.setWalletValue(view.wallet.getDecimalInput())
                dialog.dismiss()
            }
            setNegativeButton(getString(R.string.cancel)) { dialog, _ -> dialog.dismiss() }
            create().show()
        }
    }

    private fun openMonthSelectionPopUp() {
        val view = layoutInflater.inflate(R.layout.month_popup_body, null)
        val pickerMonth = setMonthPicker(view)
        val pickerYear = setYearPicker(view)

        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.apply {
            setTitle(getString(R.string.month_selection_popup_message))
            setCancelable(true)
            setView(view)
            setPositiveButton(getString(R.string.confirm)) { dialog, _ ->
                monthViewModel.setMonth(pickerMonth.value + 1, pickerYear.value)
                dialog.dismiss()
            }
            setNegativeButton(getString(R.string.cancel)) { dialog, _ -> dialog.dismiss() }
            create().show()
        }
    }

    private fun setMonthPicker(view: View) : NumberPicker {
        val pickerMonth = view.picker_month
        with(pickerMonth) {
            minValue = 0
            maxValue = months.size - 1
            displayedValues = months
            value = monthViewModel.month.value?.monthNumber.orZero() - 1
        }
        return pickerMonth
    }

    private fun setYearPicker(view: View) : NumberPicker {
        val pickerYear = view.picker_year
        with(pickerYear) {
            minValue = 2019
            maxValue = 2025
            value = monthViewModel.month.value?.year.orZero()
        }
        return pickerYear
    }

    private fun switchBetweenListAndReport(item: MenuItem) {
        if (supportFragmentManager.findFragmentById(R.id.content_fragment) is MovementsFragment) {
            replaceContentFragment(ReportFragment())
            item.setIcon(R.drawable.ic_format_list_bulleted_white_24dp)
            item.setTitle(R.string.action_list)
        } else {
            replaceContentFragment(MovementsFragment())
            item.setIcon(R.drawable.ic_report_white_24dp)
            item.setTitle(R.string.action_report)
        }
    }

    private fun replaceContentFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.content_fragment, fragment)
            .commit()
    }

    fun addContentFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.content_fragment, fragment)
            .addToBackStack(fragment.javaClass.simpleName)
            .commit()
    }
}
