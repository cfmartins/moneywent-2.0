package com.cristinaffmartins.moneywent.view.fragments

import android.os.Bundle
import android.view.*
import android.view.MenuItem.SHOW_AS_ACTION_ALWAYS
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.cristinaffmartins.moneywent.R
import com.cristinaffmartins.moneywent.database.AppDatabase
import com.cristinaffmartins.moneywent.database.Movement
import com.cristinaffmartins.moneywent.repository.MovementRepository
import com.cristinaffmartins.moneywent.utils.MovementCategories
import com.cristinaffmartins.moneywent.utils.getDecimalInput
import com.cristinaffmartins.moneywent.utils.getTextInput
import com.cristinaffmartins.moneywent.viewmodel.MovementsViewModel
import com.cristinaffmartins.moneywent.viewmodel.MovementsViewModelFactory
import kotlinx.android.synthetic.main.fragment_edit_movement.*

class EditMovementFragment : Fragment() {

    private var movementType: String? = null
    private var movementId: Long = -1L
    private var isIncome = false

    private lateinit var movementsViewModel: MovementsViewModel
    private lateinit var movement: Movement

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            movementType = it.getString(MOVEMENT_TYPE)
            movementId = it.getLong(MOVEMENT_ID)
        }
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_edit_movement, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        MovementsViewModelFactory(MovementRepository(AppDatabase.getInstance(requireContext()).movementDao())).also {
            movementsViewModel = ViewModelProviders.of(this, it).get(MovementsViewModel::class.java)
        }

        if(movementId != -1L) {
            movementsViewModel.getMovementById(movementId).observe(viewLifecycleOwner, Observer { movement ->
                this.movement = movement
                populateView(movement)
            })
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        menu.clear()
        menu.add(R.string.cancel).also {
            it.setIcon(R.drawable.ic_close_white_24dp)
            it.setShowAsAction(SHOW_AS_ACTION_ALWAYS)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.title == getString(R.string.cancel)) {
            activity?.supportFragmentManager?.popBackStackImmediate()
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }

    private fun populateView(movement: Movement?) {
        category_spinner.setSelection(resources.getStringArray(R.array.categories).indexOf(movement?.category.toString()))
        description.editText?.setText(movement?.description)
        value.editText?.setText(movement?.value.toString())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (movementType == EXPENSE) {
            title.text = getString(R.string.expense)
        } else {
            title.text = getString(R.string.income)
            isIncome = true
        }
        save_button.setOnClickListener {
            if(validateFields()) {
                addOrEditMovement()
                fragmentManager?.popBackStackImmediate()
            }
        }
    }

    private fun validateFields(): Boolean {
        return description.getTextInput().isNotEmpty() && value.getTextInput().isNotEmpty()
    }

    private fun addOrEditMovement() {
        val category = MovementCategories.valueOf(category_spinner.selectedItem.toString())
        val description = description.getTextInput()
        val value = value.getDecimalInput()
        if (movementId != -1L) {
            movement.apply {
                this.category = category
                this.description = description
                this.value = value
            }
            movementsViewModel.editMovement(movement)
        } else {
            movementsViewModel.addMovement(category, description, isIncome, value)
        }
    }

    companion object {
        private const val MOVEMENT_TYPE = "movement_type"
        private const val MOVEMENT_ID = "movement_id"
        const val EXPENSE = "EXPENSE"
        const val INCOME = "INCOME"

        fun newInstance(movementType: String?, movementId: Long = -1L) =
            EditMovementFragment().apply {
                arguments = Bundle().apply {
                    putString(MOVEMENT_TYPE, movementType)
                    putLong(MOVEMENT_ID, movementId)
                }
            }
    }
}
