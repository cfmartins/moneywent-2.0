package com.cristinaffmartins.moneywent.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.cristinaffmartins.moneywent.R
import com.cristinaffmartins.moneywent.database.AppDatabase
import com.cristinaffmartins.moneywent.repository.MovementRepository
import com.cristinaffmartins.moneywent.viewmodel.ReportViewModel
import com.cristinaffmartins.moneywent.viewmodel.ReportViewModelFactory
import kotlinx.android.synthetic.main.fragment_report.*

class ReportFragment : Fragment() {
    private lateinit var reportViewModel: ReportViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_report, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        ReportViewModelFactory(MovementRepository(AppDatabase.getInstance(requireContext()).movementDao())).also {
            reportViewModel = ViewModelProviders.of(this, it).get(ReportViewModel::class.java)
        }

        reportViewModel.totalBalance.observe(this, Observer {
            total_balance.text = getString(R.string.total_balance, it)
        })

        reportViewModel.totalIncome.observe(this, Observer {
            total_income.text = getString(R.string.total_income, it)
        })

        reportViewModel.totalExpenses.observe(this, Observer {
            total_expenses.text = getString(R.string.total_expenses, it)
        })

        reportViewModel.difference.observe(this, Observer {
            difference.text = getString(R.string.difference, it)
        })

        reportViewModel.moneySaved.observe(this, Observer {
            money_saved.text = getString(R.string.money_saved, it.moneySaved, it.moneySavedPercentage)
        })
    }
}
