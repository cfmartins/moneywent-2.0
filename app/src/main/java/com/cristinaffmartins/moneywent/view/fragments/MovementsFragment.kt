package com.cristinaffmartins.moneywent.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.cristinaffmartins.moneywent.R
import com.cristinaffmartins.moneywent.database.AppDatabase
import com.cristinaffmartins.moneywent.database.Movement
import com.cristinaffmartins.moneywent.repository.MovementRepository
import com.cristinaffmartins.moneywent.utils.MovementsRecyclerViewAdapter
import com.cristinaffmartins.moneywent.view.activities.MainActivity
import com.cristinaffmartins.moneywent.view.dialogs.AddMovementDialog
import com.cristinaffmartins.moneywent.view.fragments.EditMovementFragment.Companion.EXPENSE
import com.cristinaffmartins.moneywent.view.fragments.EditMovementFragment.Companion.INCOME
import com.cristinaffmartins.moneywent.viewmodel.MovementsViewModel
import com.cristinaffmartins.moneywent.viewmodel.MovementsViewModelFactory
import kotlinx.android.synthetic.main.fragment_movements.*

class MovementsFragment : Fragment(), MovementsRecyclerViewAdapter.OnMovementClickListener{

    private lateinit var movementsViewModel: MovementsViewModel
    private lateinit var movementsRecyclerViewAdapter: MovementsRecyclerViewAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_movements, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        MovementsViewModelFactory(MovementRepository(AppDatabase.getInstance(requireContext()).movementDao())).also {
            movementsViewModel = ViewModelProviders.of(this, it).get(MovementsViewModel::class.java)
        }
        movement_list.layoutManager = LinearLayoutManager(activity)
        movementsRecyclerViewAdapter = MovementsRecyclerViewAdapter(emptyList(), context, this)
        movement_list.adapter = movementsRecyclerViewAdapter

        val itemTouchHelper = ItemTouchHelper(MovementsRecyclerViewAdapter.SwipeToDeleteCallback(movementsRecyclerViewAdapter))
        itemTouchHelper.attachToRecyclerView(movement_list)

        movementsViewModel.getMovementListForSelectedMonth().observe(this, Observer<List<Movement>> { movementList ->
            if (movementList.isEmpty()) {
                no_movements_message.visibility = View.VISIBLE
                movement_list.visibility = View.GONE
            } else {
                movementsRecyclerViewAdapter.setMovements(movementList)
                no_movements_message.visibility = View.GONE
                movement_list.visibility = View.VISIBLE
            }
        })

        fab.setOnClickListener { AddMovementDialog().show(requireFragmentManager(), null) }
    }

    override fun onMovementClick(movement: Movement) {
        val editMovementFragment = EditMovementFragment.newInstance(if (movement.isIncome) INCOME else EXPENSE, movement.movementId)
        (activity as MainActivity).addContentFragment(editMovementFragment)
    }

    override fun onMovementSwipe(movementList: List<Movement>, position: Int) {
        val alertDialogBuilder = AlertDialog.Builder(requireContext())
        alertDialogBuilder.apply {
            setTitle(getString(R.string.delete_confirmation))
            setCancelable(false)
            alertDialogBuilder.setPositiveButton(getString(R.string.delete)) { dialog, _ ->
                movementsViewModel.deleteMovement(movementList[position])
                movementsRecyclerViewAdapter.notifyItemRemoved(position)
                dialog.dismiss()
            }
            setNegativeButton(getString(R.string.cancel)) { dialog, _ ->
                movementsRecyclerViewAdapter.notifyDataSetChanged()
                dialog.dismiss()
            }
            create()
            show()
        }
    }
}
