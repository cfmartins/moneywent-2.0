package com.cristinaffmartins.moneywent.view.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricPrompt
import com.cristinaffmartins.moneywent.R
import kotlinx.android.synthetic.main.activity_splash_screen.*
import java.util.concurrent.Executors

class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        val executor = Executors.newSingleThreadExecutor()
        val activity = this
        val biometricPrompt = BiometricPrompt(activity, executor, object : BiometricPrompt.AuthenticationCallback() {
            override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                super.onAuthenticationSucceeded(result)
                val intent = Intent(activity, MainActivity::class.java)
                startActivity(intent)
                finishAfterTransition()
            }
        })

        val promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle(getString(R.string.app_name))
            .setSubtitle(getString(R.string.authentication_prompt))
            .setDeviceCredentialAllowed(true)
            .build()

        biometricPrompt.authenticate(promptInfo)

        authenticate_button.setOnClickListener {
            biometricPrompt.authenticate(promptInfo)
        }
    }
}
