package com.cristinaffmartins.moneywent.repository

import com.cristinaffmartins.moneywent.database.Month
import com.cristinaffmartins.moneywent.database.MonthDao
import io.reactivex.Single

class MonthRepository(private val monthDao: MonthDao) {

    fun addMonth(month: Month): Single<Long> = Single.fromCallable { monthDao.addMonth(month) }

    fun getMonth(monthNumber: Int, year: Int): Single<Month> = Single.fromCallable { monthDao.getMonth(monthNumber, year) }

    fun getMonthById(monthId: Long): Single<Month> = Single.fromCallable { monthDao.getMonthById(monthId) }
}