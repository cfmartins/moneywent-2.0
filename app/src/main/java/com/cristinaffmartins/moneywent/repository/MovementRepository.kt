package com.cristinaffmartins.moneywent.repository

import com.cristinaffmartins.moneywent.database.Movement
import com.cristinaffmartins.moneywent.database.MovementDao
import io.reactivex.Completable
import io.reactivex.Single

class MovementRepository(private val movementDao: MovementDao) {

    fun getMovementsByMonthId(monthId: Long) = movementDao.getMovementsByMonthId(monthId)

    fun getMovementById(movementId: Long) = movementDao.getMovementById(movementId)

    fun getIncomesByMonthId(monthId: Long) : Single<List<Movement>> = Single.fromCallable { movementDao.getIncomesByMonthId(monthId) }

    fun getExpensesByMonthId(monthId: Long) : Single<List<Movement>> = Single.fromCallable { movementDao.getExpensesByMonthId(monthId) }

    fun addMovement(movement: Movement): Completable = Completable.fromAction { movementDao.addMovement(movement) }

    fun editMovement(movement: Movement) : Completable = Completable.fromAction { movementDao.editMovement(movement) }

    fun deleteMovement(movement: Movement) : Completable = Completable.fromAction { movementDao.deleteMovement(movement)}
}