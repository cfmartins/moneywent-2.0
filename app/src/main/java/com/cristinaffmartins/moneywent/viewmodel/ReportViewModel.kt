package com.cristinaffmartins.moneywent.viewmodel

import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cristinaffmartins.moneywent.repository.MovementRepository
import com.cristinaffmartins.moneywent.utils.SharedPreferencesHelper
import com.cristinaffmartins.moneywent.utils.SharedPreferencesHelper.ACCOUNT_BALANCE_KEY
import com.cristinaffmartins.moneywent.utils.SharedPreferencesHelper.MEAL_CARD_BALANCE_KEY
import com.cristinaffmartins.moneywent.utils.SharedPreferencesHelper.SAVINGS_KEY
import com.cristinaffmartins.moneywent.utils.SharedPreferencesHelper.SELECTED_MONTH_KEY
import com.cristinaffmartins.moneywent.utils.SharedPreferencesHelper.WALLET_KEY
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class ReportViewModel(private val movementRepository: MovementRepository) : ViewModel() {

    val totalBalance: LiveData<Float>
        get() = totalBalanceLiveData

    val totalIncome: LiveData<Float>
        get() = totalIncomeLiveData

    val totalExpenses: LiveData<Float>
        get() =  totalExpensesLiveData

    val difference: LiveData<Float>
        get() = differenceLiveData

    val moneySaved: LiveData<MoneySaved>
        get() {
            return moneySavedLiveData
        }

    private val totalBalanceLiveData = MutableLiveData<Float>()
    private val totalIncomeLiveData = MutableLiveData<Float>()
    private val totalExpensesLiveData = MutableLiveData<Float>()
    private val differenceLiveData = MutableLiveData<Float>()
    private val moneySavedLiveData = MutableLiveData<MoneySaved>()
    private val compositeDisposable = CompositeDisposable()

    private val sharedPreferenceChangeListener = SharedPreferences.OnSharedPreferenceChangeListener { sharedPreferences, key ->
        if (key == ACCOUNT_BALANCE_KEY || key == SAVINGS_KEY || key == MEAL_CARD_BALANCE_KEY || key == WALLET_KEY) {
            updateTotalBalance(sharedPreferences)
        } else if (key == SELECTED_MONTH_KEY) {
            updateTotalIncome(sharedPreferences)
            updateTotalExpenses(sharedPreferences)
        }
    }

    init {
        SharedPreferencesHelper.sharedPreferences.registerOnSharedPreferenceChangeListener(sharedPreferenceChangeListener)
        updateTotalBalance(SharedPreferencesHelper.sharedPreferences)
        updateTotalIncome(SharedPreferencesHelper.sharedPreferences)
        updateTotalExpenses(SharedPreferencesHelper.sharedPreferences)
    }

    override fun onCleared() {
        super.onCleared()
        SharedPreferencesHelper.sharedPreferences.unregisterOnSharedPreferenceChangeListener(sharedPreferenceChangeListener)
        compositeDisposable.clear()
    }

    private fun updateTotalBalance(sharedPreferences: SharedPreferences) {
        val account = sharedPreferences.getFloat(ACCOUNT_BALANCE_KEY, 0F)
        val savings = sharedPreferences.getFloat(SAVINGS_KEY, 0F)
        val mealCard = sharedPreferences.getFloat(MEAL_CARD_BALANCE_KEY, 0F)
        val wallet = sharedPreferences.getFloat(WALLET_KEY, 0F)
        totalBalanceLiveData.value = account + savings + mealCard + wallet
        updateDifference()
    }

    private fun updateTotalIncome(sharedPreferences: SharedPreferences) {
        val month = sharedPreferences.getLong(SELECTED_MONTH_KEY, 0L)
        movementRepository.getIncomesByMonthId(month)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { movementList ->
                totalIncomeLiveData.value = movementList.sumByDouble { it.value.toDouble() }.toFloat()
                updateDifference()
                updateMoneySaved()
            }
            .also { compositeDisposable.add(it) }
    }

    private fun updateTotalExpenses(sharedPreferences: SharedPreferences) {
        val month = sharedPreferences.getLong(SELECTED_MONTH_KEY, 0L)
        movementRepository.getExpensesByMonthId(month)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { movementList ->
                totalExpensesLiveData.value = movementList.sumByDouble { it.value.toDouble() }.toFloat()
                updateDifference()
                updateMoneySaved()
            }
            .also { compositeDisposable.add(it) }
    }

    private fun updateDifference() {
        val totalBalance = totalBalanceLiveData.value ?: 0F
        val totalIncome = totalIncomeLiveData.value ?: 0F
        val totalExpenses = totalExpensesLiveData.value ?: 0F
        differenceLiveData.value = totalBalance - (totalIncome - totalExpenses)
    }

    private fun updateMoneySaved() {
        val totalIncome = totalIncomeLiveData.value ?: 0F
        val totalExpenses = totalExpensesLiveData.value ?: 0F
        val moneySaved = totalIncome - totalExpenses
        if (totalIncome != 0F && moneySaved > 0) {
            moneySavedLiveData.value = MoneySaved(moneySaved, (moneySaved / totalIncome * 100).toInt())
        } else {
            moneySavedLiveData.value = MoneySaved(moneySaved, 0)
        }
    }

    data class MoneySaved(val moneySaved: Float, val moneySavedPercentage: Int)
}
