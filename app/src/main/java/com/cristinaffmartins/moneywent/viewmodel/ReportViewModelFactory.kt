package com.cristinaffmartins.moneywent.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.cristinaffmartins.moneywent.repository.MovementRepository

class ReportViewModelFactory(private val movementRepository: MovementRepository) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ReportViewModel::class.java)) {
            return ReportViewModel(movementRepository) as T
        }
        throw IllegalArgumentException()
    }
}