package com.cristinaffmartins.moneywent.viewmodel

import androidx.lifecycle.ViewModel
import com.cristinaffmartins.moneywent.utils.SharedPreferencesHelper
import com.cristinaffmartins.moneywent.utils.SharedPreferencesHelper.ACCOUNT_BALANCE_KEY
import com.cristinaffmartins.moneywent.utils.SharedPreferencesHelper.MEAL_CARD_BALANCE_KEY
import com.cristinaffmartins.moneywent.utils.SharedPreferencesHelper.SAVINGS_KEY
import com.cristinaffmartins.moneywent.utils.SharedPreferencesHelper.WALLET_KEY

class BalanceViewModel : ViewModel() {

    fun getAccountValue() = SharedPreferencesHelper.read(ACCOUNT_BALANCE_KEY, 0F)

    fun setAccountValue(value: Float) = SharedPreferencesHelper.write(ACCOUNT_BALANCE_KEY, value)

    fun getSavingsValue() = SharedPreferencesHelper.read(SAVINGS_KEY, 0F)

    fun setSavingsValue(value: Float) = SharedPreferencesHelper.write(SAVINGS_KEY, value)

    fun getMealCardValue() = SharedPreferencesHelper.read(MEAL_CARD_BALANCE_KEY, 0F)

    fun setMealCardValue(value: Float) = SharedPreferencesHelper.write(MEAL_CARD_BALANCE_KEY, value)

    fun getWalletValue() = SharedPreferencesHelper.read(WALLET_KEY, 0F)

    fun setWalletValue(value: Float) = SharedPreferencesHelper.write(WALLET_KEY, value)
}