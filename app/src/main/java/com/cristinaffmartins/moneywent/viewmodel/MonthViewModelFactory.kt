package com.cristinaffmartins.moneywent.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.cristinaffmartins.moneywent.repository.MonthRepository

class MonthViewModelFactory(private val monthRepository: MonthRepository) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MonthViewModel::class.java)) {
            return MonthViewModel(monthRepository) as T
        }
        throw IllegalArgumentException()
    }
}