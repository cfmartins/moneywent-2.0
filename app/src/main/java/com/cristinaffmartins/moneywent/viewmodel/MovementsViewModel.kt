package com.cristinaffmartins.moneywent.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.cristinaffmartins.moneywent.database.Movement
import com.cristinaffmartins.moneywent.repository.MovementRepository
import com.cristinaffmartins.moneywent.utils.MovementCategories
import com.cristinaffmartins.moneywent.utils.SharedPreferencesHelper
import io.reactivex.schedulers.Schedulers

class MovementsViewModel(private val movementRepository: MovementRepository) : ViewModel() {

    fun getMovementListForSelectedMonth() : LiveData<List<Movement>> {
        val monthId = SharedPreferencesHelper.read(SharedPreferencesHelper.SELECTED_MONTH_KEY, 1)
        return movementRepository.getMovementsByMonthId(monthId)
    }

    fun getMovementById(movementId: Long) : LiveData<Movement> {
        return movementRepository.getMovementById(movementId)
    }

    fun addMovement(category: MovementCategories, description: String, isIncome: Boolean, value: Float) {
        val monthId = SharedPreferencesHelper.read(SharedPreferencesHelper.SELECTED_MONTH_KEY, 1)
        val movement = Movement(category, description, monthId, isIncome, value)
        movementRepository.addMovement(movement).subscribeOn(Schedulers.io()).onErrorComplete().subscribe()
    }

    fun editMovement(movement: Movement) {
        movementRepository.editMovement(movement).subscribeOn(Schedulers.io()).onErrorComplete().subscribe()
    }

    fun deleteMovement(movement: Movement) {
        movementRepository.deleteMovement(movement).subscribeOn(Schedulers.io()).onErrorComplete().subscribe()
    }
}