package com.cristinaffmartins.moneywent.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.cristinaffmartins.moneywent.repository.MovementRepository

class MovementsViewModelFactory(private val movementRepository: MovementRepository) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MovementsViewModel::class.java)) {
            return MovementsViewModel(movementRepository) as T
        }
        throw IllegalArgumentException()
    }
}