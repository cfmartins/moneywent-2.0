package com.cristinaffmartins.moneywent.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cristinaffmartins.moneywent.database.Month
import com.cristinaffmartins.moneywent.repository.MonthRepository
import com.cristinaffmartins.moneywent.utils.SharedPreferencesHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MonthViewModel(private val monthRepository: MonthRepository) : ViewModel()  {

    val month: LiveData<Month>
        get() = monthLiveData

    private val monthLiveData = MutableLiveData<Month>()
    private val compositeDisposable = CompositeDisposable()

    init {
        val monthId = SharedPreferencesHelper.read(SharedPreferencesHelper.SELECTED_MONTH_KEY, 0)
        monthRepository.getMonthById(monthId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ month ->
                monthLiveData.postValue(month)
            }, {
                addNewMonth(1, 2019)
            })
            .also { compositeDisposable.add(it) }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    fun setMonth(monthNumber: Int, year: Int) {
        monthRepository.getMonth(monthNumber, year)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ month ->
                handleMonth(month)
            }, {
                addNewMonth(monthNumber, year)
            })
            .also { compositeDisposable.add(it) }
    }

    private fun handleMonth(month: Month) {
        monthLiveData.postValue(month)
        SharedPreferencesHelper.write(SharedPreferencesHelper.SELECTED_MONTH_KEY, month.monthId)
    }

    private fun addNewMonth(monthNumber: Int, year: Int) {
        val month = Month(monthNumber, year)
        monthRepository.addMonth(month)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { monthId ->
                monthId?.let {
                    monthLiveData.postValue(month)
                    SharedPreferencesHelper.write(SharedPreferencesHelper.SELECTED_MONTH_KEY, it)
                }
            }.also { compositeDisposable.add(it) }
    }
}