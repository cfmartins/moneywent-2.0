package com.cristinaffmartins.moneywent.utils

enum class MovementCategories {
    SALARY, GROCERIES, DINING, CLOTHES, CAR, TRAVEL, SAVINGS, PARTY, HEALTH, GYM, OTHER
}