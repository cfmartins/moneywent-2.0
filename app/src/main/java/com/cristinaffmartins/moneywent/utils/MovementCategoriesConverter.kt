package com.cristinaffmartins.moneywent.utils

import androidx.room.TypeConverter

class MovementCategoriesConverter {

    @TypeConverter
    fun convertToMovementCategories(value: Int) : MovementCategories {
        return when(value) {
            1 -> MovementCategories.SALARY
            2 -> MovementCategories.GROCERIES
            3 -> MovementCategories.DINING
            4 -> MovementCategories.CLOTHES
            5 -> MovementCategories.CAR
            6 -> MovementCategories.TRAVEL
            7 -> MovementCategories.SAVINGS
            8 -> MovementCategories.PARTY
            9 -> MovementCategories.HEALTH
            10 -> MovementCategories.GYM
            11 -> MovementCategories.OTHER
            else -> MovementCategories.OTHER
        }
    }

    @TypeConverter
    fun convertToInt(movementCategories: MovementCategories) : Int {
        return when(movementCategories) {
            MovementCategories.SALARY -> 1
            MovementCategories.GROCERIES -> 2
            MovementCategories.DINING -> 3
            MovementCategories.CLOTHES -> 4
            MovementCategories.CAR -> 5
            MovementCategories.TRAVEL -> 6
            MovementCategories.SAVINGS -> 7
            MovementCategories.PARTY -> 8
            MovementCategories.HEALTH -> 9
            MovementCategories.GYM -> 10
            MovementCategories.OTHER -> 11
        }
    }
}