package com.cristinaffmartins.moneywent.utils

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.cristinaffmartins.moneywent.database.AppDatabase
import com.cristinaffmartins.moneywent.database.Month
import com.cristinaffmartins.moneywent.database.Movement
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope

class DatabaseWorker(context: Context, workerParams: WorkerParameters) : CoroutineWorker(context, workerParams) {

    override val coroutineContext = Dispatchers.IO

    override suspend fun doWork(): Result = coroutineScope {
        try {
            val database = AppDatabase.getInstance(applicationContext)

            val month = Month(1, 2019)
            database.monthDao().addMonth(month)

            val anotherMonth = Month(2, 2019)
            database.monthDao().addMonth(anotherMonth)

            val movement = Movement(MovementCategories.CAR, "Something I bought", 1, false, 10f)
            database.movementDao().addMovement(movement)

            val anotherMovement = Movement(MovementCategories.SALARY, "Another thing I bought", 2, true,100f)
            database.movementDao().addMovement(anotherMovement)

            Result.success()
        } catch (ex: Exception) {
            Result.failure()
        }
    }

}