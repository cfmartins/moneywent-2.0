package com.cristinaffmartins.moneywent.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.cristinaffmartins.moneywent.R
import com.cristinaffmartins.moneywent.database.Movement
import kotlinx.android.synthetic.main.movement_list_item.view.*

class MovementsRecyclerViewAdapter(private var movementList: List<Movement>, private val context: Context?, private val listener: OnMovementClickListener):
    RecyclerView.Adapter<MovementsRecyclerViewAdapter.MovementViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovementViewHolder {
        return MovementViewHolder(LayoutInflater.from(context).inflate(R.layout.movement_list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return movementList.size
    }

    override fun onBindViewHolder(holder: MovementViewHolder, position: Int) {
        val movementItem = movementList[position]
        holder.categoryIconImageView.setImageResource(getCategoryIcon(movementItem.category))
        holder.descriptionTextView.text = movementItem.description
        if (movementItem.isIncome) {
            holder.valueTextView.setTextColor(ContextCompat.getColor(context!!, R.color.income_value))
            holder.valueTextView.text = context.getString(R.string.income_value, movementItem.value)
        } else {
            holder.valueTextView.setTextColor(ContextCompat.getColor(context!!, R.color.expense_value))
            holder.valueTextView.text = context.getString(R.string.expense_value, movementItem.value)
        }
        holder.itemView.setOnClickListener { listener.onMovementClick(movementItem) }
    }

    private fun getCategoryIcon(category: MovementCategories) : Int {
        return when(category) {
            MovementCategories.SALARY -> R.drawable.ic_salary_white_24dp
            MovementCategories.GROCERIES -> R.drawable.ic_groceries_white_24dp
            MovementCategories.DINING -> R.drawable.ic_food_white_24dp
            MovementCategories.CLOTHES -> R.drawable.ic_clothes_white_24dp
            MovementCategories.CAR -> R.drawable.ic_car_white_24dp
            MovementCategories.TRAVEL -> R.drawable.ic_travel_white_24dp
            MovementCategories.SAVINGS -> R.drawable.ic_savings_white_24dp
            MovementCategories.PARTY -> R.drawable.ic_party_white_24dp
            MovementCategories.HEALTH -> R.drawable.ic_health_white_24dp
            MovementCategories.GYM -> R.drawable.ic_gym_white_24dp
            MovementCategories.OTHER -> R.drawable.ic_money_white_24dp
        }
    }

    fun setMovements(movement: List<Movement>) {
        movementList = movement
        notifyDataSetChanged()
    }

    private fun deleteItem(position: Int) {
        listener.onMovementSwipe(movementList, position)
    }

    class MovementViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val categoryIconImageView: ImageView = itemView.categoryIcon
        val descriptionTextView: TextView = itemView.description
        val valueTextView: TextView = itemView.value
    }

    interface OnMovementClickListener {
        fun onMovementClick(movement: Movement)
        fun onMovementSwipe(movementList: List<Movement>, position: Int)
    }

     class SwipeToDeleteCallback(private val adapter: MovementsRecyclerViewAdapter) : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

         override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            adapter.deleteItem(viewHolder.adapterPosition)
         }

         override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean = false
    }
}