package com.cristinaffmartins.moneywent.utils

import com.google.android.material.textfield.TextInputLayout

fun TextInputLayout.getDecimalInput() : Float {
    return this.editText?.text.toString().toFloat()
}

fun TextInputLayout.getTextInput() : String {
    return this.editText?.text.toString()
}

fun Int?.orZero(): Int = this ?: 0