package com.cristinaffmartins.moneywent.utils

import android.content.Context
import android.content.SharedPreferences

object SharedPreferencesHelper {
    lateinit var sharedPreferences: SharedPreferences

    private const val SHARED_PREFERENCES_NAME = "moneywent_prefs"

    const val SELECTED_MONTH_KEY = "selected_month"
    const val ACCOUNT_BALANCE_KEY = "account_balance"
    const val SAVINGS_KEY = "savings"
    const val MEAL_CARD_BALANCE_KEY = "meal_card_balance"
    const val WALLET_KEY = "wallet"

    fun init(context: Context) {
        sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)
    }

    fun read(key: String, defaultValue: Long) = sharedPreferences.getLong(key, defaultValue)

    fun write(key: String, value: Long) {
        with(sharedPreferences.edit()) {
            putLong(key, value)
            apply()
        }
    }

    fun read(key: String, defaultValue: Float) = sharedPreferences.getFloat(key, defaultValue)

    fun write(key: String, value: Float) {
        with(sharedPreferences.edit()) {
            putFloat(key, value)
            apply()
        }
    }
}